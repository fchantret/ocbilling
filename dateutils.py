import collections
import datetime

class DateUtils(object):
    # Static properties
    __months_dict = collections.OrderedDict()
    __months_dict['janvier'] = 1
    __months_dict['février'] = 2
    __months_dict['mars'] = 3
    __months_dict['avril'] = 4
    __months_dict['mai'] = 5
    __months_dict['juin'] = 6
    __months_dict['juillet'] = 7
    __months_dict['août'] = 8
    __months_dict['septembre'] = 9
    __months_dict['octobre'] = 10
    __months_dict['novembre'] = 11
    __months_dict['décembre'] = 12

    __month_names = list(__months_dict.keys())

    @staticmethod
    def get_month_name(month_num):
        return DateUtils.__month_names[month_num - 1]

    @staticmethod
    def get_month_num(month_name):
        return DateUtils.__months_dict[month_name]

    @staticmethod
    def get_previous_month():
        today = datetime.date.today()
        first = today.replace(day=1)
        lastMonth = first - datetime.timedelta(days=1)

        month = {}
        month['num'] = int(lastMonth.strftime('%m'))
        month['year'] = int(lastMonth.strftime('%Y'))
        return month

    @staticmethod
    def get_current_month():
        today = datetime.date.today()

        month = {}
        month['num'] = int(today.strftime('%m'))
        month['year'] = int(today.strftime('%Y'))
        return month
