from datetime import datetime
from dateutils import DateUtils
from ocapi import OCApi

import configparser
import getpass

# -----------------------------------------------------------------------------


def prompt(message, allowed_values, default_value):
    choice = None
    while choice not in allowed_values:
        choice = input(message).lower()
        if (choice == ""):
            choice = default_value

    return choice

# Load or create the configuration file
config = configparser.ConfigParser()
config.read('config.ini')
write_conf = False

if 'email' in config['DEFAULT']:
    email = config['DEFAULT']['email']
else:
    write_conf = True
    email = input("Adresse email ou nom d'utilisateur OC: ")
    config['DEFAULT']['email'] = email

if 'password' in config['DEFAULT']:
    password = config['DEFAULT']['password']
else:
    password = getpass.getpass(prompt="Mot de passe OC: ", stream=None)
    choice = prompt("Voulez vous sauvegarder le mot de passe en clair dans le fichier config.ini [o/N] ? "
                    , ['o', 'n'], 'n')
    if choice == 'o':
        config['DEFAULT']['password'] = password
        write_conf = True

# Connection to OC
print('Login... ', end='', flush=True)
oc = OCApi(email, password)
oc.login()
print('OK')

# Write the configuration file
if write_conf is True:
    with open('config.ini', 'w') as configfile:
        config.write(configfile)

# Choose the month for which to compute the billing
choice = prompt('Voulez vous calculer la facturation: \n'
                '1. Du mois précédent\n'
                '2. Du mois en cours\n'
                'Votre choix [1]: '
                , ['1', '2'], '1')

if choice == "1":
    month = DateUtils.get_previous_month()
else:
    month = DateUtils.get_current_month()

# Start the computation
month_name = DateUtils.get_month_name(month['num'])
print("Facturation de", month_name, month['year'])

date_min = datetime.strptime(str(month['year']) + '-' + str(month['num']) + '-01 00:00:00', '%Y-%m-%d %H:%M:%S')

if month['num'] == 12:
    date_max = datetime.strptime(str(month['year'] + 1) + '-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
else:
    date_max = datetime.strptime(str(month['year']) + '-' + str(month['num'] + 1) + '-01 00:00:00', '%Y-%m-%d %H:%M:%S')

# Get the first page of history
page = 1
print('Récupération de la page ' + str(page) + '... ', end='', flush=True)
sessions = oc.get_history(page)
print('OK')

# Fetch extra pages of history while we did not reach the beginning of the previous month
while sessions[-1]['date'] > date_min:
    page += 1
    print('Récupération de la page ' + str(page) + '... ', end='', flush=True)
    sessions += oc.get_history(page)
    print('OK')

students = {}

tarif = [30, 35, 40]

ef_rel = [0, 0, 0]
ef_ns  = [0, 0, 0]
af_rel = [0, 0, 0]
af_ns  = [0, 0, 0]
orl_rel = [0, 0, 0]
orl_ns = [0, 0, 0]

for session in sessions:
    # Skip sessions from current month
    if session['date'] >= date_max:
        continue

    # Stop from sessions to the month before the one we want the billing
    if session['date'] < date_min:
        break

    if session['oral'] == True:
        if session['status'] == 'Réalisée':
            orl_rel[session['level'] - 1] += 1
        elif session['status'] == 'Étudiant absent':
            orl_ns[session['level'] - 1] += 1
        continue

    # Fetch the student status if not already in cache
    if session['student_id'] not in students:
        student = {}
        student['name'] = session['student_name']
        print('Récupération du statut de l\'élève', session['student_name'] + '... ', end='', flush=True)
        student['status'] = oc.get_student_status(session['student_id'])
        print(student['status'])
        if student['status'] == "Auto-financé":
            choice = prompt("Confirmer [O/n] ? ", ['o', 'n'], 'o')
            if choice == 'n':
                student['status'] = "Financé par un tiers"
                print('Récupération du statut de l\'élève', session['student_name'] + '... ', end='', flush=True)
                print(student['status'])

        students[session['student_id']] = student

    if students[session['student_id']]['status'] == 'Financé par un tiers':
        if session['status'] == 'Réalisée':
            ef_rel[session['level'] - 1] += 1
        elif session['status'] == 'Étudiant absent':
            ef_ns[session['level'] - 1] += 1
    else:
        if session['status'] == 'Réalisée':
            af_rel[session['level'] - 1] += 1
        elif session['status'] == 'Étudiant absent':
            af_ns[session['level'] - 1] += 1

total = 0

print()

nb_af = 0
nb_ef = 0

for student in students.values():
    if student['status'] == 'Auto-financé':
        nb_af += 1
    else:
        nb_ef += 1


print('Autofinancé(s) :', nb_af)
print('Financé(s)     :', nb_ef)
print('Sessions       :', sum(ef_rel) + sum(af_rel) + sum(orl_rel))
print()

for i in range(len(af_rel)):
    print('Auto-financé - Session de niveau', i+1, ':', af_rel[i])
    total += af_rel[i] * tarif[i] / 2
print()

for i in range(len(af_ns)):
    print('Auto-financé - No-show de niveau', i+1, ':', af_ns[i])
    total += af_ns[i] * tarif[i] / 4
print()

print('Nombre de forfaits auto-financé:', nb_af)
total += nb_af * 30
print()

for i in range(len(ef_rel)):
    print('Financé - Session de niveau', i+1, ':', ef_rel[i])
    total += ef_rel[i] * tarif[i]
print()

for i in range(len(ef_ns)):
    print('Financé - No-show de niveau', i+1, ':', ef_ns[i])
    total += ef_ns[i] * tarif[i] / 2
print()

for i in range(len(orl_rel)):
    print('Soutenance - Session de niveau', i+1, ':', orl_rel[i])
    total += orl_rel[i] * tarif[i]
print()

for i in range(len(orl_ns)):
    print('Soutenance - No-show de niveau', i+1, ':', orl_ns[i])
    total += orl_ns[i] * tarif[i] / 2
print()

print('Total:', total, '€')
