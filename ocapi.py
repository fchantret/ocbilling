import html
import json
import re
import requests

from datetime import datetime
from dateutils import DateUtils

class OCApi(object):
    __base_url = 'https://openclassrooms.com/'

    rx_sessions = re.compile(
        r'<a href="/fr/mentorship/sessions/\d+">\s*(\d+) (\w+) (\d+) à (\d+:\d+)\s*</a>[^<]?</td>[^<]?' # date
         '<td>[^<]?<a href="/fr/mentorship/students/(\d+)/dashboard">([^<]+)</a>[^<]?</td>[^<]?' # student id / name
         '<td>([^<]+)</td>[^<]?' # session status
         '<td class="[^"]+">[^<]?(?:<span class="[^"]+">(Soutenance)</span>)?[^<]?</td>[^<]?' # oral
         '<td class="[^"]+">[^<]?<span class="[^"]+">[^<]?([^<]+)[^<]?</span>[^<]?</td>' # level
    )
    rx_student_status = re.compile(r'<div class="mentorshipStudent__details oc-typography-body1"><p>([^<]+)</p>')

    def __init__(self, email, password):
        self.__email = email
        self.__password = password
        self.__session_id = None

    def login(self):
        if self.__session_id == None:
            # Get CSRF
            url = OCApi.__base_url + 'fr/login_ajax'
            r = requests.get(url)
            if r.status_code != 200:
                raise RuntimeError(f'{r.url} returned {r.status_code}')

            # Login
            url = OCApi.__base_url + 'login_check'
            headers = {'x-requested-with': 'XMLHttpRequest'}
            cookies = dict(PHPSESSID=r.cookies['PHPSESSID'])
            payload = {'_username': self.__email, '_password': self.__password, 'state': r.json()['csrf']}

            r = requests.post(url, headers=headers, cookies=cookies, data=payload)
            if r.status_code != 200:
                raise RuntimeError(f'{r.url} returned {r.status_code}')

            self.__session_id = r.cookies['PHPSESSID']

            # Print the sessionId to use it in development to avoid being block temporarily by calling too much the login_check
            print('sessionId:', self.__session_id)

    def get_history(self, num_page = 1):
        url = OCApi.__base_url + 'fr/mentorship/dashboard/mentorship-sessions-history?page=' + str(num_page)
        cookies = dict(PHPSESSID=self.__session_id)

        r = requests.get(url, cookies=cookies)
        if r.status_code != 200:
            raise RuntimeError(f'{r.url} returned {r.status_code}')

        text = r.text.replace('\n', '')

        date_frmt = "{}-{:02d}-{:02d} {}:00"
        sessions = []

        for match in OCApi.rx_sessions.findall(text):
            session = {}

            dt = date_frmt.format(match[2], DateUtils.get_month_num(match[1]), int(match[0]), match[3])
            session['date'] = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
            session['student_id'] = match[4]
            session['student_name'] = html.unescape(match[5])
            session['status'] = match[6]
            session['oral'] = True if match[7] != '' else False
            session['level'] = int(match[8])
            sessions.append(session)

        return sessions

    def get_student_status(self, student_id):
        url = OCApi.__base_url + 'fr/mentorship/students/' + student_id + '/dashboard'
        cookies = dict(PHPSESSID=self.__session_id)

        r = requests.get(url, cookies=cookies)
        if r.status_code != 200:
            raise RuntimeError(f'{r.url} returned {r.status_code}')

        html = r.text.replace('\n', '')

        match = OCApi.rx_student_status.search(html)
        if match != None:
            return match.group(1).strip()
